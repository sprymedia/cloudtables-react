"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

class CloudTables extends _react.default.Component {
  componentDidMount() {
    const script = document.createElement("script");

    if (this.props.src) {
      script.src = this.props.src;
    }

    if (this.props.apiKey) {
      script.setAttribute("data-apiKey", this.props.apiKey);
    }

    if (this.props.clientId) {
      script.setAttribute("data-clientId", this.props.clientId);
    }

    if (this.props.token) {
      script.setAttribute("data-token", this.props.token);
    }

    if (this.props.clientName) {
      script.setAttribute("data-clientName", this.props.clientName);
    }

    this.instance.appendChild(script);
  }

  render() {
    return /*#__PURE__*/_react.default.createElement("div", {
      ref: el => this.instance = el
    }, null); // return <div ref={(el) => (this.instance = el)} />;
  }

}

exports.default = CloudTables;