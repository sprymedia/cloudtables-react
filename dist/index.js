"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "CloudTables", {
  enumerable: true,
  get: function get() {
    return _cloudtables.default;
  }
});
exports.default = void 0;

var _cloudtables = _interopRequireDefault(require("./components/cloudtables"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _default = _cloudtables.default;
exports.default = _default;