import logo from "./logo.svg";
import "./App.css";
import CloudTables from "./lib/components/cloudtables";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <CloudTables
          src="https://ct-examples.cloudtables.io/loader/4e9e8e3c-f448-11eb-8a3f-43eceac3195f/table/d"
          apiKey="AzG0e04UxhduaTAJjYC3Dgfr"
        ></CloudTables>
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}
export default App;
