import React from "react";

export default class CloudTables extends React.Component {
  componentDidMount() {
    const script = document.createElement("script");
    if (this.props.src) {
      script.src = this.props.src;
    }
    if (this.props.apiKey) {
      script.setAttribute("data-apiKey", this.props.apiKey);
    }
    if (this.props.clientId) {
      script.setAttribute("data-clientId", this.props.clientId);
    }
    if (this.props.token) {
      script.setAttribute("data-token", this.props.token);
    }
    if (this.props.clientName) {
      script.setAttribute("data-clientName", this.props.clientName);
    }
    this.instance.appendChild(script);
  }
  render() {
    return React.createElement(
      "div",
      { ref: (el) => (this.instance = el) },
      null
    );
    // return <div ref={(el) => (this.instance = el)} />;
  }
}
